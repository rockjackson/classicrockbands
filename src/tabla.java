import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

import java.sql.*;

public class tabla extends JFrame {

    //CONNECT THE DATA BASE
   /* static String usuari =
    static String clau =
    static String urlDades =*/



    public static ResultSet resposta;
    public static Connection conexio;
    public static Statement pregunta;


    public static DefaultTableModel modelTableProducts = new DefaultTableModel();

    public JButton butt1 = new JButton("1");
    public JButton butt2 = new JButton("next");
    public JButton butt3 = new JButton("prev");
    public JButton butt4 = new JButton("ult");


    public String nom;
    public String titulo;
    public String estilo;
    public int durad;

    static String[] info = new String[4];


    public tabla() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        conexio = DriverManager.getConnection(urlDades, usuari, clau);
        pregunta = conexio.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        resposta = pregunta.executeQuery("SELECT * FROM albuns inner join grups on albuns.grup=grups.id_grup;");

        setLayout(new FlowLayout());

        final String[] nomCol = {"titulo", "grupo", "estilo", "duracion"};

        for (int i = 0; nomCol.length > i; i++) {
            modelTableProducts.addColumn(nomCol[i]);
        }

        JTable table = new JTable(modelTableProducts);
        JScrollPane scroll = new JScrollPane(table);
        add(scroll);

        JPanel panBut = new JPanel();

        butt1.addActionListener(actionEvent -> {
            try {
                if (resposta.first()) {
                    modelTableProducts.addRow(getInfo());
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });

        butt2.addActionListener(actionEvent -> {
            try {
                if (resposta.next()) {
                    modelTableProducts.addRow(getInfo());
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });
        butt3.addActionListener(actionEvent -> {
            try {
                if (resposta.previous()) {
                    modelTableProducts.addRow(getInfo());
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });
        butt4.addActionListener(actionEvent -> {
            try {
                if (resposta.last()) {
                    modelTableProducts.addRow(getInfo());
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });

        panBut.add(butt1);
        panBut.add(butt2);
        panBut.add(butt3);
        panBut.add(butt4);

        add(panBut);

        setSize(900, 500);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        new tabla();
    }
    public String[] getInfo() throws SQLException {
        modelTableProducts.setRowCount(0);
        estilo = resposta.getString("estilo");
        titulo = resposta.getString("titulo");
        nom = resposta.getString("nom");
        durad = resposta.getInt("duracion");

        info[0] = titulo;
        info[1] = nom;
        info[2] = estilo;
        info[3] = String.valueOf(durad);

        return info;
    }
}
